# Path to the log file where debugging information will be written
LOG_FILE_DIR = 'data/output/logs/debug.log'

# Path to the file containing cookies (input)
COOKIE_FILE_DIR = 'data/input/cookies.inpt'

# Timezone setting for the application
TIMEZONE = 'Asia/Saigon'

# Timeout duration for HTTP requests in seconds
TIMEOUT = 600

# Whether to enable JavaScript execution in web scraping
USE_JAVASCRIPT = False

# Whether to use cookies in web scraping
USE_COOKIE = True

# Whether to use a custom user agent in web scraping
USE_USER_AGENT = True

# Logging level for debugging
LOG_LEVEL = 3

# Hostname or base URL for API requests
HOSTNAME = 'http://207.246.102.93'

# API paths for Facebook page and ad data
API_FACEBOOK_PAGE_PATH = '/api/v1/facebook-pages'
API_FACEBOOK_AD_PATH = '/api/v1/facebook-ads'

# URL for a distraction page (used in web scraping)
DISTRACTION_URL = 'https://www.google.com'

# URL for managing cookies (used in web scraping)
COOKIE_URL = 'https://www.facebook.com'

# Timeout duration for page load in milliseconds
PAGE_LOAD_TIMEOUT = 500

# Implicit wait time in milliseconds
IMPLICITLY_WAIT = 500

# Request timeout in milliseconds
REQUEST_TIMEOUT = 500

# Repeat interval for the scraper in seconds
REPEAT_INTERVAL = 1

# Whether to ignore certificate errors in HTTPS requests
IGNORE_CERTIFICATE_ERRORS = True

# Whether to ignore SSL errors in HTTPS requests
IGNORE_SSL_ERRORS = True

# Whether to run the web browser in headless mode (no GUI)
HEADLESS = False

# Log file extension
LOG_EXT = '.txt'

# Disable sandboxing for the web browser
NO_SANDBOX = True

# Disable using /dev/shm for shared memory
DISABLE_DEV_SHM_USAGE = True

# Enable automation in the web browser
ENABLE_AUTOMATION = True

# Disable automation extensions in the web browser
DISABLE_AUTOMATION_EXTENSION = True

# Disable JavaScript execution in the web browser
DISABLE_JAVASCRIPT = False

# Detach the web browser process
DETACH = True

# Language code for the Chrome browser
CHROME_LANGUAGE_CODE = 'en,en_US'

# Path to the Chrome driver executable on Windows
DEFAULT_WINDOWS_CHROME_DRIVER_PATH = 'libs/chromedriver.exe'

# Path to the Chrome driver executable on Ubuntu
DEFAULT_UBUNTU_CHROME_DRIVER_PATH = '/usr/lib/chromium-browser/chromedriver'

# Default URL for Shopify
DEFAULT_SHOPIFY = 'https://www.shopify.com'

# Scraper's cycle duration in hours
SCRAPING_CYCLE_HOURS = 24

# Base URL for Facebook Ad Library
AD_LIBRARY_BASE_URL = 'https://www.facebook.com/ads/library/?active_status=all&ad_type=all&country=US&impression_search_field=has_impressions_lifetime&view_all_page_id='

# MongoDB server URL
MONGODB_SERVER = 'mongodb://127.0.0.1'

# MongoDB server port
MONGODB_PORT = 27017

# MongoDB database name
MONGODB_DB = 'facebook_ad'

# MongoDB collection name for Shopify products
MONGODB_COLLECTION = 'shopify_products'

# Maximum number of products to scrape
PRODUCT_LIMIT = 50

# Delay duration in seconds
DELAY = 10
