import platform
import random
import sys
import time
import cfscrape
import requests
import settings
import urllib3
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from utils.io import DataIO

# Disable insecure request warning from urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class Downloader:
    """
    This class provides methods for downloading web content, handling HTTP requests, and interacting with Selenium WebDriver.

    Attributes:
        scraper_arguments (dict): A dictionary containing scraper arguments.
    """

    def __init__(self, scraper_arguments):
        # Initialize the Downloader with scraper_arguments
        self.scraper_arguments = scraper_arguments

    def initialize_chrome_driver(self):
        """
        Initialize a Chrome WebDriver with configured options.

        Returns:
            webdriver.Chrome: Initialized Chrome WebDriver.
        """
        chrome_driver = None

        try:
            chrome_options = Options()

            # Configure Chrome options based on settings

            # Add argument to ignore certificate errors
            if settings.IGNORE_CERTIFICATE_ERRORS:
                chrome_options.add_argument('--ignore-certificate-errors')

            # Add argument to ignore SSL errors
            if settings.IGNORE_SSL_ERRORS:
                chrome_options.add_argument('--ignore-ssl-errors')

            # Enable headless mode if specified
            if settings.HEADLESS:
                chrome_options.add_argument("--headless")

            # Set Chrome log level if specified
            if settings.LOG_LEVEL:
                chrome_options.add_argument(
                    '--log-level=' + str(settings.LOG_LEVEL))

            # Disable sandbox mode if specified
            if settings.NO_SANDBOX:
                chrome_options.add_argument('--no-sandbox')

            # Disable /dev/shm usage if specified
            if settings.DISABLE_DEV_SHM_USAGE:
                chrome_options.add_argument('--disable-dev-shm-usage')

            # Set Chrome language preferences if specified
            if settings.CHROME_LANGUAGE_CODE:
                chrome_options.add_argument('--lang=en-US')
                chrome_options.add_argument('--lang=en-GB')
                chrome_options.add_experimental_option(
                    'prefs', {'intl.accept_languages': settings.CHROME_LANGUAGE_CODE})

            # Exclude automation if specified
            if settings.ENABLE_AUTOMATION:
                chrome_options.add_experimental_option(
                    "excludeSwitches", ['enable-automation'])

            # Disable automation extension if specified
            if settings.DISABLE_AUTOMATION_EXTENSION:
                chrome_options.add_experimental_option(
                    'useAutomationExtension', False)

            # Disable JavaScript if specified
            if settings.DISABLE_JAVASCRIPT:
                chrome_options.add_argument("--disable-javascript")
                chrome_options.add_experimental_option("prefs", {
                    'profile.managed_default_content_settings.javascript': 2,
                    "webkit.webprefs.javascript_enabled": False,
                    "profile.content_settings.exceptions.javascript.*.setting": 2,
                    "profile.default_content_setting_values.javascript": 2
                })

            # Enable experimental cookie flags if specified
            if settings.USE_COOKIE:
                experimental_flags = [
                    'same-site-by-default-cookies@1', 'cookies-without-same-site-must-be-secure@1']
                chromeLocal_state_prefs = {
                    'browser.enabled_labs_experiments': experimental_flags}
                chrome_options.add_experimental_option(
                    'localState', chromeLocal_state_prefs)

            # Set a custom User-Agent if specified
            if settings.USE_USER_AGENT:
                user_agent = DataIO().generate_user_agent()
                chrome_options.add_argument('--user-agent=%s' % user_agent)

            # Enable detach mode if specified
            if settings.DETACH:
                chrome_options.add_experimental_option("detach", True)

            # Initialize Chrome WebDriver based on the platform
            if platform.system() == 'Windows':
                chrome_driver = webdriver.Chrome(
                    settings.DEFAULT_WINDOWS_CHROME_DRIVER_PATH, chrome_options=chrome_options)
            elif platform.system() == 'Linux':
                chrome_driver = webdriver.Chrome(
                    settings.DEFAULT_UBUNTU_CHROME_DRIVER_PATH, chrome_options=chrome_options)

            # Set page load timeout if specified
            if settings.PAGE_LOAD_TIMEOUT:
                chrome_driver.set_page_load_timeout(
                    settings.PAGE_LOAD_TIMEOUT)

            # Set implicitly wait time if specified
            if settings.IMPLICITLY_WAIT:
                chrome_driver.implicitly_wait(settings.IMPLICITLY_WAIT)
        except Exception as e:
            # Handle exceptions and log errors
            chrome_driver = None
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return chrome_driver

    def initialize_session(self, cookies_list=list(), proxy_settings={}, user_agent_str=list()):
        """
        Initialize an HTTP request session with specified settings.

        This method initializes an HTTP request session with optional cookies, proxy settings, and a User-Agent string.

        Args:
            cookies_list (list): List of cookies.
            proxy_settings (dict): Proxy settings.
            user_agent_str (str): User-Agent string.

        Returns:
            requests.Session: Initialized HTTP request session.
        """
        session = None
        try:
            headers = dict()
            session = requests.Session()

            # Add cookies if specified
            if settings.USE_COOKIE and len(cookies_list) != 0:
                # Find 'c_user' and 'xs' cookies if available
                c_user_cookie = next(
                    (cookie for cookie in cookies_list if cookie.get('name', str()) == 'c_user'), dict())
                xs_cookie = next(
                    (cookie for cookie in cookies_list if cookie.get('name', str()) == 'xs'), dict())
                cookies = {
                    'c_user': c_user_cookie.get('value', str()),
                    'xs': xs_cookie.get('value', str()),
                }
                session.cookies.update(cookies)

            # Add User-Agent header if specified
            if settings.USE_USER_AGENT:
                user_agent_str = DataIO().generate_user_agent()
                headers.update({
                    'Content-Type': 'text/html; charset=utf-8',
                    'User-Agent': user_agent_str,
                })
                session.headers.update(headers)
        except Exception as e:
            # Handle exceptions and log errors
            session = None
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return session

    def fetch_chrome_page(self, chrome_driver, url):
        """
        Fetch a web page using a Chrome WebDriver.

        Args:
            chrome_driver (webdriver.Chrome): Initialized Chrome WebDriver.
            url (str): URL of the web page to fetch.

        Returns:
            webdriver.Chrome: Updated Chrome WebDriver.
        """
        try:
            # Sleep for a random time before making the request
            time.sleep(random.randint(5, 10))
            # Visit the URL with Chrome WebDriver
            chrome_driver.get(url)
            # Sleep for a random time after the request
            time.sleep(random.randint(5, 10))
        except Exception as e:
            # Handle exceptions and log errors
            chrome_driver = None
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return chrome_driver

    def close_chrome_driver(self, chrome_driver):
        """
        Close a Chrome WebDriver if it's open.

        Args:
            chrome_driver (webdriver.Chrome): Chrome WebDriver to close.

        Returns:
            None
        """
        try:
            if hasattr(chrome_driver, 'close') and callable(chrome_driver.close):
                chrome_driver.close()
        except Exception as e:
            # Handle exceptions and log errors
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)

    def fetch_web_page(self, http_request, url):
        """
        Fetch a web page using an HTTP request session.

        Args:
            http_request (requests.Session): Initialized HTTP request session.
            url (str): URL of the web page to fetch.

        Returns:
            requests.Response: HTTP response of the fetched page.
        """
        try:
            # Send an HTTP GET request to the URL with a specified timeout
            http_response = http_request.get(
                url, timeout=settings.TIMEOUT, verify=False)
        except Exception as e:
            # Handle exceptions and log errors
            http_response = None
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return http_response

    def fetch_cloudflare_page(self, url):
        """
        Fetch a web page protected by Cloudflare using cfscrape.

        Args:
            url (str): URL of the web page to fetch.

        Returns:
            requests.Response: HTTP response of the fetched page.
        """
        try:
            # Use cfscrape to bypass Cloudflare protection
            cfscraper = cfscrape.CloudflareScraper(delay=settings.DELAY)
            http_response = cfscraper.get(url)
        except Exception as e:
            # Handle exceptions and log errors
            http_response = None
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return http_response
