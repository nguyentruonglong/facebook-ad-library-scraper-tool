from dataclasses import dataclass

# Class representing a Facebook Page
@dataclass(init=True)
class FacebookPage:
    """
    Data class representing a Facebook Page.
    """
    facebook_page_id: str = str()  # Unique ID of the Facebook Page
    facebook_page_url: str = str()  # URL of the Facebook Page
    facebook_page_name: str = str()  # Name of the Facebook Page
    last_crawling: str = str()  # Date of the last crawling
    page_status: bool = True  # Status of the page (active by default)
    facebook_ads_info: dict = dict()  # Information about Facebook Ads associated with the Page

# Data class representing a Facebook Ad
@dataclass(init=True)
class FacebookAd:
    """
    Data class representing a Facebook Ad.
    """
    advertising_id: str = str()  # Unique ID of the ad
    started_running: str = str()  # Date when the ad started running
    embedded_video_url: str = str()  # URL of embedded video in the ad
    video_thumbnail_url: str = str()  # URL of the video thumbnail
    advertised_product_url: str = str()  # URL of the advertised product
    price: float = float(0.0)  # Price associated with the ad
    created_at: str = str()  # Date when the ad was created
    updated_at: str = str()  # Date when the ad was last updated

# Data class representing an E-commerce Store
@dataclass(init=True)
class EcommerceStore:
    """
    Data class representing an E-commerce Store.
    """
    store_id: str = str()  # Unique ID of the store
    store_url: str = str()  # URL of the store
    store_status: bool = False  # Status of the store (inactive by default)
    crawled_at: str = str()  # Date when the store was last crawled

# Data class representing a Shopify Product
@dataclass(init=True)
class ShopifyProduct:
    """
    Data class representing a Shopify Product.
    """
    store_url: str = str()  # URL of the store associated with the product
    store_id: str = str()  # Unique ID of the store
    product_id: str = str()  # Unique ID of the product
    product_title: str = str()  # Title of the product
    product_url: str = str()  # URL of the product
    price: float = float(0.0)  # Price of the product
    created_at: str = str()  # Date when the product was created
    updated_at: str = str()  # Date when the product was last updated
