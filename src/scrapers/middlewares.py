import json
import re
import sys
import unicodedata as ud
import pytz
import requests
import settings
import time
import urllib
import validators
import dateutil.parser
import urllib3

from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from furl.furl import furl
from nested_lookup.nested_lookup import nested_lookup
from multiprocessing.dummy import Pool as ThreadPool

from utils.io import DataIO
from .downloader import Downloader
from .items import EcommerceStore, FacebookAd, ShopifyProduct
from .pipelines import MongoDBPipeline

# Disable InsecureRequestWarning from urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class DownloaderMiddleware:
    """
    Middleware for processing HTTP responses and managing cookies in web scraping.

    This class provides methods for cleaning and fixing data, processing HTML and JSON responses,
    and handling Facebook cookies using a Selenium Chrome driver.

    Attributes:
        scraper_arguments (dict): A dictionary containing scraper arguments.
    """

    def __init__(self, scraper_arguments):
        """
        Initialize the DownloaderMiddleware with the given scraper arguments.

        Args:
            scraper_arguments (dict): A dictionary containing scraper arguments.
        """
        self.scraper_arguments = scraper_arguments

    def remove_invalid_characters(self, input_str):
        """
        Remove invalid characters from the input string and normalize it.

        Args:
            input_str (str): The input string to process.

        Returns:
            str: The cleaned and normalized input string.
        """
        try:
            # Normalize the input string
            input_str = ud.normalize('NFKC', input_str)
            # Replace invalid characters with space
            input_str = re.sub(r'[^0-9a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹäöüßÄÖÜẞ]+', ' ', input_str)
            # Replace multiple spaces with a single space
            input_str = re.sub(r'\s+', ' ', input_str)
            # Remove leading and trailing spaces
            input_str = input_str.strip()
        except Exception as e:
            input_str = str()
            current_class = self.__class__.__name__
            # Log any errors that occur during processing
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return input_str

    def fix_invalid_json(self, json_str):
        """
        Fix invalid JSON in the given JSON string.

        This method attempts to fix issues in a JSON string and returns the fixed JSON as a Python dictionary.

        Args:
            json_str (str): The JSON string to fix.

        Returns:
            dict: The fixed JSON as a Python dictionary.
        """
        while True:
            try:
                # Attempt to parse the JSON string
                fixed_json = json.loads(json_str)
                break
            except json.JSONDecodeError as e:
                if 'Expecting , delimiter' not in str(e):
                    # If the error is not related to a missing comma, raise the exception
                    raise

                # Find the position of the unexpected character
                unexpected_char_position = int(re.findall(r'\(char (\d+)\)', str(e))[0])

                # Find the position of the unescaped double quote before the unexpected character
                unescaped_quote_position = json_str.rfind(r'"', 0, unexpected_char_position)

                # Add escape characters to fix the JSON
                json_str = json_str[:unescaped_quote_position] + r'\"' + json_str[unescaped_quote_position + 1:]

                # Find the position of the next unescaped double quote after the unexpected character
                closing_quote_position = json_str.find(r'"', unescaped_quote_position + 2)

                # Add escape characters to fix the JSON
                json_str = json_str[:closing_quote_position] + r'\"' + json_str[closing_quote_position + 1:]

        # Return the fixed JSON as a Python dictionary
        return fixed_json

    def process_html_response(self, http_response):
        """
        Process HTML response from an HTTP response object.

        Args:
            http_response (Response): The HTTP response object.

        Returns:
            str: The HTML page source as a string.
        """
        try:
            page_source = str()
            if hasattr(http_response, 'content'):
                # Extract the content and decode it as UTF-8
                page_source = http_response.content
                page_source = page_source.decode('utf-8')
        except Exception as e:
            page_source = str()
            current_class = self.__class__.__name__
            # Log any errors that occur during processing
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return page_source

    def process_json_response(self, http_response):
        """
        Process JSON response from an HTTP response object.

        This method takes an HTTP response object, extracts JSON content from it, and returns it as a Python dictionary.

        Args:
            http_response (Response): The HTTP response object.

        Returns:
            dict: The JSON response as a Python dictionary.
        """
        try:
            json_response = {}

            # Check if the HTTP response object has 'content' attribute
            if hasattr(http_response, 'content'):
                # Parse the content of the response using BeautifulSoup
                soup = BeautifulSoup(http_response.content, 'html.parser')
                json_response = soup.get_text()
            elif hasattr(http_response, 'text'):
                # If 'content' is not available, use the 'text' attribute
                json_response = http_response.text

            # Fix any potential issues with the extracted JSON
            json_response = self.fix_invalid_json(json_response)

        except Exception as e:
            # Handle exceptions and initialize an empty dictionary for JSON response
            json_response = {}
            current_class = self.__class__.__name__

            # Log any errors that occur during processing
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the processed JSON response as a dictionary
            return json_response

    def process_facebook_cookie(self, chrome_driver):
        """
        Process Facebook cookies using a Selenium Chrome driver.

        Args:
            chrome_driver (WebDriver): The Chrome driver instance.

        Returns:
            WebDriver: The Chrome driver with added Facebook cookies.
        """
        try:
            chrome_driver.get(settings.COOKIE_URL)
            # Load and add Facebook cookies to the driver
            facebook_cookies = DataIO().load_cookies(settings.COOKIE_FILE_DIR)
            for cookie in facebook_cookies:
                cookie.pop('sameSite')
                chrome_driver.add_cookie(cookie)
        except Exception as e:
            current_class = self.__class__.__name__
            # Log any errors that occur during processing
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return chrome_driver


class ShopifyMiddleware(Downloader, DownloaderMiddleware):
    """
    Middleware specific to Shopify store scraping.

    This class extends the Downloader and DownloaderMiddleware classes and provides methods for
    processing Shopify store data, including extracting product details.
    """

    def __init__(self):
        super().__init__()

    def check_store_status(self, store_url):
        """
        Check the status of a Shopify store.

        Args:
            store_url (str): The URL of the Shopify store.

        Returns:
            bool: True if the store is active, False otherwise.
        """
        try:
            # Construct the URL to fetch Shopify store product data
            query_url = f'{store_url}/products.json?limit={settings.PRODUCT_LIMIT}'
            
            # Fetch the data using Cloudflare protection bypass
            http_response = self.fetch_cloudflare_page(query_url)
            
            # Introduce a delay before further processing
            time.sleep(2)
            
            # Process the HTML response into a string
            html_content = self.process_html_response(http_response)
            
            # Remove HTML tags from the HTML content
            clean_html_content = re.sub(re.compile(r'(<.*?>)'), str(), html_content)
            
            # Parse the JSON response
            json_response = json.loads(clean_html_content)
            
            # Check if 'products' key exists in the JSON response
            if 'products' in json_response:
                return True
            return False
        except Exception as e:
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
            return False

    def extract_product_id(self, product_object):
        """
        Extract the product ID from a Shopify product object.

        Args:
            product_object (dict): The product object.

        Returns:
            str: The extracted product ID.
        """
        try:
            # Extract the product ID from the product object
            product_id = str(product_object.get('id', str()))
            print(f'Product ID: {product_id}')
        except Exception as e:
            # Handle any exceptions and log errors
            product_id = str()
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return product_id

    def extract_product_title(self, product_object):
        """
        Extract the product title from a Shopify product object.

        Args:
            product_object (dict): The product object.

        Returns:
            str: The extracted product title.
        """
        try:
            # Extract the product title from the product object
            product_title = product_object.get('title', str())
            # Remove invalid characters from the title
            cleaned_product_title = self.remove_invalid_characters(product_title)
        except Exception as e:
            # Handle any exceptions and log errors
            cleaned_product_title = str()
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return cleaned_product_title

    def extract_product_price(self, product_object):
        """
        Extract the product price from a Shopify product object.

        Args:
            product_object (dict): The product object.

        Returns:
            float: The extracted product price.
        """
        try:
            # Extract the list of prices from the product variants
            price_list = nested_lookup(
                'price', product_object.get('variants', dict()))
            # Convert the first price in the list to float, or 0.0 if empty
            extracted_price = float(price_list[0]) if len(
                price_list) > 0 else float(0.0)
        except Exception as e:
            # Handle any exceptions and log errors
            extracted_price = float(0.0)
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return extracted_price

    def extract_creation_date(self, product_object):
        """
        Extract the creation date of a Shopify product.

        Args:
            product_object (dict): The product object.

        Returns:
            str: The extracted creation date.
        """
        try:
            # Extract and parse the creation date from the product object
            creation_date = str(product_object.get('created_at', str()))
            creation_date = dateutil.parser.parse(creation_date).astimezone(
                pytz.timezone(settings.TIMEZONE))
        except Exception as e:
            # Handle any exceptions and log errors
            creation_date = str()
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return creation_date

    def extract_last_update_date(self, product_object):
        """
        Extract the last update date of a Shopify product.

        Args:
            product_object (dict): The product object.

        Returns:
            str: The extracted last update date.
        """
        try:
            # Extract and parse the last update date from the product object
            last_update_date = str(product_object.get('updated_at', str()))
            last_update_date = dateutil.parser.parse(last_update_date).astimezone(
                pytz.timezone(settings.TIMEZONE))
        except Exception as e:
            # Handle any exceptions and log errors
            last_update_date = str()
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return last_update_date

    def extract_product_handle(self, product_object):
        """
        Extract the product handle from a Shopify product object.

        Args:
            product_object (dict): The product object.

        Returns:
            str: The extracted product handle.
        """
        try:
            # Extract the product handle from the product object
            product_handle = str(product_object.get('handle', str()))
            # URL-decode the product handle and handle any encoding errors
            product_handle = urllib.parse.unquote_plus(
                product_handle, encoding='utf-8', errors='replace')
            print(f'Product handle: {product_handle}')
        except Exception as e:
            # Handle any exceptions and log errors
            product_handle = str()
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return product_handle

    def extract_store_id(self, html_page_source):
        """
        Extract the store ID from the HTML page source.

        Args:
            html_page_source (str): The HTML page source.

        Returns:
            str: The extracted store ID.
        """
        try:
            store_id = str()
            # Find and extract the store ID from the HTML page source
            store_id_substrings = re.findall(
                r'"shopId":[0-9]{5,15}', html_page_source)
            store_id = store_id_substrings[0].replace(
                '"shopId":', str()) if len(store_id_substrings) > 0 else str()
        except Exception as e:
            # Handle any exceptions and log errors
            store_id = str()
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return store_id

    def parse_product_info(self, product_object):
        """
        Parse product information from a Shopify product object.

        Args:
            product_object (dict): The product object.

        Returns:
            dict: A dictionary containing parsed product information.
        """
        try:
            parsed_product = dict()
            # Create a ShopifyProduct instance and convert it to a dictionary
            parsed_product = ShopifyProduct(
                product_id=self.extract_product_id(product_object),
                product_title=self.extract_product_title(product_object),
                product_url=self.extract_product_handle(product_object),
                price=self.extract_product_price(product_object),
                created_at=self.extract_creation_date(product_object),
                updated_at=self.extract_last_update_date(product_object),
            ).__dict__

        except Exception as e:
            parsed_product = dict()
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            # Log the error with class name and line number
            DataIO().write_to_log(error_message)
        finally:
            return parsed_product

    def parse_product_items(self, json_response):
        """
        Parse product items from the JSON response.

        Args:
            json_response (dict): The JSON response containing product data.

        Returns:
            list: A list of parsed product items.
        """
        try:
            parsed_items = list()
            # Extract the 'products' list from the JSON response
            product_items = json_response.get('products', list())
            for product_item in product_items:
                # Parse each product item and add it to the list
                parsed_item_object = self.parse_product_info(product_item)
                parsed_items.append(parsed_item_object)
        except Exception as e:
            parsed_items = list()
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            # Log the error with class name and line number
            DataIO().write_to_log(error_message)
        finally:
            return parsed_items

    def scrape_product_info(self, store_url):
        """
        Scrape product information from a Shopify store URL.

        Args:
            store_url (str): The URL of the Shopify store.

        Returns:
            list: A list of scraped product items.
        """
        try:
            page_number = 0
            product_list = list()
            while True:
                page_number += 1
                # Construct the API URL for retrieving product data
                api_url = f'{store_url}/products.json?limit={settings.PRODUCT_LIMIT}&page={str(page_number)}'
                # Fetch the page using Cloudflare bypass
                http_response = self.fetch_cloudflare_page(api_url)
                # Process the JSON response
                json_response = self.process_json_response(http_response)
                # Parse the product items and add them to the list
                product_items = self.parse_product_items(json_response)
                if len(product_items) == 0:
                    break
                for product_item in product_items:
                    product_list.append(product_item)
        except Exception as e:
            product_list = list()
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            # Log the error with class name and line number
            DataIO().write_to_log(error_message)
        finally:
            return product_list

    def extract_redirected_url(self, store_url):
        """
        Extract the redirected URL from the store URL.

        Args:
            store_url (str): The URL of the store.

        Returns:
            str: The redirected URL.
        """
        try:
            # Send a GET request to the store URL and extract the final, redirected URL
            redirected_url = furl(requests.get(store_url, timeout=settings.TIMEOUT).url).origin
        except Exception as e:
            # If an exception occurs, use the original store URL
            redirected_url = store_url
            # Log the error with class name and line number
            DataIO().write_to_log(e, self.scraper.log_filepath)
        finally:
            return redirected_url

    def extract_store_info(self, store_url):
        """
        Extract store information from a store URL.

        Args:
            store_url (str): The URL of the store.

        Returns:
            dict: A dictionary containing store information.
        """
        ecommerce_store_info = dict()
        try:
            # Fetch the store page using Cloudflare bypass
            http_response = self.fetch_cloudflare_page(store_url)
            # Process the HTML response to extract store information
            page_source = self.process_html_response(http_response)
            ecommerce_store_info = EcommerceStore(
                store_id=self.extract_store_id(page_source),
                store_url=store_url,
                store_status=True,
                crawled_at=DataIO().get_current_time()
            ).__dict__
        except Exception as e:
            ecommerce_store_info = dict()
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            # Log the error with class name and line number
            DataIO().write_to_log(error_message)
        finally:
            return ecommerce_store_info

    def process_output_items(self, output_items, store_url):
        """
        Process the output items by adding store information to each item.

        Args:
            output_items (list): A list of product items.
            store_url (str): The URL of the store.

        Returns:
            list: A list of processed product items.
        """
        try:
            store_info = self.extract_store_info(store_url)
            for item in output_items:
                product_url = item.get('product_url', str())
                # Update the 'product_url' with the full product URL
                product_url = f'{store_url}/products/{product_url}'
                item.update({'product_url': product_url})
                # Update each item with store information
                item.update(store_info)
        except Exception as e:
            output_items = list()
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            # Log the error with class name and line number
            DataIO().write_to_log(error_message)
        finally:
            return output_items

    def process_shopify_products(self, input_url):
        """
        Process Shopify products based on the input URL.

        Args:
            input_url (str): The input URL to process.

        Returns:
            dict: A dictionary containing Shopify product information.
        """
        try:
            current_time = DataIO().get_current_time()
            negative_time = (current_time - timedelta(days=10000)).strftime('%Y-%m-%d %H:%M:%S.%f')

            # Check if the input URL is a valid URL
            if not validators.url(input_url):
                return {
                    'created_at': negative_time,
                    'updated_at': negative_time,
                }

            # Parse the input URL using the 'furl' library
            url_structure = furl(input_url.strip())
            # Remove query parameters and fragments from the URL
            url_structure = url_structure.remove(args=True, fragment=True)
            # Extract the origin (protocol + domain) from the URL
            home_url = str(url_structure.origin)
            # Extract the host (domain) from the URL
            host_url = str(url_structure.host)
            # Extract the path (URL path after the domain) from the URL
            path_url = str(url_structure.path)

            # Extract the redirected URL from the home URL
            redirected_url = self.extract_redirected_url(home_url)

            # Create an instance of the MongoDBPipeline
            mongodb_pipeline = MongoDBPipeline()

            # Extract the product handle from the path URL
            handle = re.sub(r'^(\/collections\/(.)+)?\/products\/', str(), path_url)
            handle = str(urllib.parse.unquote_plus(handle))

            # Create an advanced product URL using the host URL and product handle
            adv_product_url = f'{host_url}/products/{handle}'

            # Query the MongoDB to check if the product item already exists
            product_item = mongodb_pipeline.query_product_item(adv_product_url)

            # If the product item doesn't exist in the database
            if product_item is not None:
                return product_item

            store_status = self.check_store_status(redirected_url)

            if not store_status:
                return {
                    'created_at': negative_time,
                    'updated_at': negative_time,
                }
            
            # Scrape all product items from the store
            product_list = self.scrape_product_info(redirected_url)
            # Process the product items, adding store information
            product_list = self.process_output_items(product_list, redirected_url)
            # Insert the product items into MongoDB for future queries
            mongodb_pipeline.insert_product_items(product_list)

            # Query the MongoDB again to get the product item
            product_item = mongodb_pipeline.query_product_item(adv_product_url)

            # If product_item is still None, set default created_at and updated_at timestamps
            if product_item is None:
                return {
                    'created_at': negative_time,
                    'updated_at': negative_time,
                }
            
            return product_item

        except Exception as e:
            # Handle exceptions and set default created_at and updated_at timestamps
            product_item = {
                'created_at': negative_time,
                'updated_at': negative_time,
            }
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            # Log the error with class name and line number
            DataIO().write_to_log(error_message)
            return product_item


class AdLibraryMiddleware(Downloader, DownloaderMiddleware):
    """
    Middleware for interacting with the Facebook Ad Library.

    This class extends the functionality of both 'Downloader' and 'DownloaderMiddleware' to interact with the
    Facebook Ad Library, download ad data, and perform related operations.
    """

    def __init__(self):
        super().__init__()

    def is_page_working(self, html_source):
        """
        Check if the page is working based on certain warnings in the HTML source.

        Args:
            html_source (str): The HTML source code of the page.

        Returns:
            bool: True if the page is working, False otherwise.
        """
        try:
            # Define a list of warning messages to check for in the HTML source
            warning_messages = ["You must log in to continue.", "This Page Isn't Available", "The link may be broken",
                                "This Content Isn't Available Right Now", "Bạn phải đăng nhập để tiếp tục."]
            
            # Check if any of the warning messages are present in the HTML source and set is_working accordingly
            is_working = not any(warning.lower() in html_source.lower() for warning in warning_messages)
        except Exception as e:
            # Handle any exceptions that may occur and set is_working to False
            is_working = False
            current_class = self.__class__.__name__
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the is_working flag, indicating whether the page is working or not
            return is_working

    def extract_advertising_id(self, html_source):
        """
        Extract the advertising ID from the HTML source.

        Args:
            html_source (str): The HTML source code of the page.

        Returns:
            str: The extracted advertising ID.
        """
        try:
            advertising_id = str()
            soup = BeautifulSoup(html_source, 'html.parser')
            # Select the HTML element with class "_4rhp" to find the advertising ID
            advertising_id_elem = soup.select_one('._4rhp')
            if advertising_id_elem:
                # Use regular expression to extract the advertising ID from the element's text
                advertising_id = re.findall('[0-9]+', advertising_id_elem.get_text())[0]
        except Exception as e:
            advertising_id = str()
            current_class = self.__class__.__name__
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return advertising_id

    def extract_start_date(self, html_source):
        """
        Extract the date the ad started running from the HTML source.

        Args:
            html_source (str): The HTML source code of the page.

        Returns:
            str: The extracted start date in a standardized format.
        """
        try:
            start_date = str()
            soup = BeautifulSoup(html_source, 'html.parser')
            # Select the HTML element with class "_9cd3" to find the start date
            start_date_elem = soup.select_one('._9cd3')
            if start_date_elem:
                # Get the text from the element and strip any extra whitespace
                start_date = start_date_elem.get_text().strip()
                # Convert the start date to a standardized format using the "convert_running_datetime" method
                start_date = self.convert_running_datetime(start_date)
        except Exception as e:
            start_date = str()
            current_class = self.__class__.__name__
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return start_date

    def extract_embedded_url(self, original_url):
        """
        Extract the embedded URL from an original URL.

        Args:
            original_url (str): The original URL.

        Returns:
            str: The extracted embedded URL.
        """
        try:
            embedded_url = str()
            # Initialize an HTTP request and send a GET request to the original URL
            http_request = self.initialize_request()
            http_response = http_request.get(original_url)
            # Process the HTML response to get the page source
            page_source = self.process_html_response(http_response)
            # Search for a JavaScript redirect in the page source
            matched_substrings = re.findall(r'document\.location\.replace\(\"[^\)]+\"\)', page_source)
            if matched_substrings:
                # Extract the middle URL from the JavaScript redirect code
                middle_url = matched_substrings[0].replace('document.location.replace("', "").replace(')', "").replace('\\', "").replace('"', "")
                # Send a request to the middle URL to get the final embedded URL
                embedded_url = requests.get(middle_url, verify=False, timeout=settings.TIMEOUT).url
        except Exception as e:
            embedded_url = str()
            current_class = self.__class__.__name__
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return embedded_url

    def extract_advertised_url(self, html_source):
        """
        Extract the advertised URL from the HTML source.

        Args:
            html_source (str): The HTML source code of the page.

        Returns:
            str: The extracted advertised URL.
        """
        try:
            advertised_url = str()
            soup = BeautifulSoup(html_source, 'html.parser')
            # Select the HTML element with class "_231w._231z._4yee[href]" to find the advertised URL
            product_link = soup.select_one('a._231w._231z._4yee[href]')
            if not product_link:
                # If the first element is not found, try finding other elements
                desc_url_link = soup.select_one('._4ik4._4ik5 a[href]')
                if not desc_url_link:
                    desc_url_link = soup.select_one('._7jyr a[href]')
                if desc_url_link:
                    # If a relevant element is found, set advertised_url to its "href" attribute
                    advertised_url = desc_url_link['href']
            else:
                # If the first element is found, set advertised_url to its "href" attribute
                advertised_url = product_link['href']

            if validators.url(advertised_url):
                # If the extracted URL is a valid URL, use the "extract_embedded_url" method to get the final URL
                advertised_url = self.extract_embedded_url(advertised_url)
                # Parse the URL using the furl library to remove query parameters and fragments
                url_structure = furl(advertised_url.strip())
                url_structure = url_structure.remove(args=True, fragment=True)
                advertised_url = str(url_structure.url)
            else:
                # If the extracted URL is not valid, use the default Shopify URL
                advertised_url = settings.DEFAULT_SHOPIFY
        except Exception as e:
            advertised_url = settings.DEFAULT_SHOPIFY
            current_class = self.__class__.__name__
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            return advertised_url

    def extract_ad_thumbnail_url(self, html_source):
        """
        Extracts the thumbnail URL of a Facebook ad from the given HTML page source.

        Args:
            html_source (str): The HTML source code of the web page.

        Returns:
            str: The URL of the ad's thumbnail image.
        """
        try:
            # Parse the HTML source code using BeautifulSoup
            soup = BeautifulSoup(html_source, 'html.parser')

            # Attempt to find the thumbnail URL within video elements
            video_thumbnail_elem = soup.select_one('._8o0a._8o0b video[poster]')
            if video_thumbnail_elem is not None:
                return video_thumbnail_elem['poster']

            # Try finding it in image elements
            image_thumbnail_elem = soup.select_one('img._7jys[src]')
            if image_thumbnail_elem is not None:
                return image_thumbnail_elem['src']

            # If not found, check embedded video elements
            embedded_video_elem = soup.select_one('._8o0a._8o0b video[src]')
            if embedded_video_elem is not None:
                return embedded_video_elem['src']

        except Exception as e:
            # Handle exceptions, log errors, and set a default value
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)

        # Default value if no thumbnail URL is found
        return str()

    def extract_facebook_page_id(self, html_source):
        """
        Extracts the Facebook page ID from the given HTML page source.

        Args:
            html_source (str): The HTML source code of the web page.

        Returns:
            str: The Facebook page ID.
        """
        patterns = [
            r'page_id=[0-9]{5,30}',
            r'page_id:"[0-9]{5,30}',
            r'sid=[0-9]{5,30}',
            r'content="fb://page/?id=[0-9]{5,30}',
            r'"pageID":"[0-9]{5,30}',
            r'pageID:"[0-9]{5,30}'
        ]

        try:
            # Initialize an empty string to store the page ID
            extracted_page_id = str()

            for pattern in patterns:
                matched_substrings = re.findall(pattern, html_source)
                if len(matched_substrings) > 0:
                    # If a match is found, extract the page ID by removing the pattern prefix
                    extracted_page_id = matched_substrings[0].split('"')[-1]  # Split by double quote and get the last part
                    break  # Exit the loop when a match is found

        except Exception as e:
            # Handle exceptions, log errors, and set a default value for extracted_page_id
            extracted_page_id = str()
            current_class_name = self.__class__.__name__
            error_message = f'({current_class_name} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)

        finally:
            # Return the extracted or default page ID
            return extracted_page_id

    def extract_facebook_page_name(self, html_source):
        """
        Extracts the Facebook page name from the given HTML page source.

        Args:
            page_source (str): The HTML source code of the web page.

        Returns:
            str: The Facebook page name.
        """
        # Define a list of CSS selectors to search for the page name
        css_selectors = [
            '.l61y9joe.fkloq7h8.kekj1cs4.rnz22s23.svz86pwt.jrvjs1jy.a53abz89',
            '#mount_0_0 > div > div:nth-child(1) > div.rq0escxv.l9j0dhe7.du4w35lb > div.rq0escxv.l9j0dhe7.du4w35lb > div > div > div.j83agx80.cbu4d94t.d6urw2fd.dp1hu0rb.l9j0dhe7.du4w35lb > div.l9j0dhe7.dp1hu0rb.cbu4d94t.j83agx80 > div:nth-child(1) > div.rq0escxv.l9j0dhe7.du4w35lb.j83agx80.pfnyh3mw.taijpn5t.gs1a9yip.owycx6da.btwxx1t3.ihqw7lf3.cddn0xzi > div > div > div > div.rq0escxv.l9j0dhe7.du4w35lb.j83agx80.cbu4d94t.g5gj957u.d2edcug0.hpfvmrgz.on77hlbc.buofh1pr.o8rfisnq.ph5uu5jm.b3onmgus.ihqw7lf3.ecm0bbzt > div > div > div:nth-child(1) > h2 > span > span',
            '#seo_h1_tag > a._64-f > span',
            'h3._52jd._52jb > span > strong > a',
            'div.nc684nl6 a strong > span',
            '#js_0 span.fwb.fcg > a',
            '#u_0_0 > span._33vv > a > span',
            '#msite-pages-header-contents ._59k._2rgt._1j-f._2rgt'
        ]

        try:
            # Initialize an empty string to store the page name
            page_name = str()
            soup = BeautifulSoup(html_source, 'html.parser')

            # Try each CSS selector to find the page name
            for css_selector in css_selectors:
                page_name_elem = soup.select_one(css_selector)
                if page_name_elem:
                    page_name = page_name_elem.get_text()
                    break  # Break when a match is found

            # If no CSS selector matches, attempt to extract the page name using a regular expression
            if not page_name:
                matched_substrs = re.findall(r'pageName:"([^}]{1,500})"}', html_source)
                if matched_substrs:
                    page_name = matched_substrs[0].replace('pageName:"', '').replace('"}}', '')

        except Exception as e:
            # Handle exceptions, log errors, and set a default value for page_name
            page_name = str()
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)

        finally:
            # Return the extracted or default page name
            return page_name

    def extract_detailed_ad_info(self, html_source):
        """
        Extract detailed information about a Facebook ad from its HTML source.

        Args:
            html_source (str): The HTML source code of the ad page.

        Returns:
            dict: A dictionary containing detailed ad information.
        """
        try:
            ad_info = dict()
            # Check if the ad is not marked as 'Inactive' in the HTML source
            if 'Inactive' not in html_source:
                # Create an AdInfo object and convert it to a dictionary
                ad_info = FacebookAd(
                    advertising_id=self.extract_advertising_id(html_source),
                    started_running=self.extract_start_date(html_source),
                    advertised_product_url=self.extract_advertised_url(html_source),
                    video_thumbnail_url=self.extract_ad_thumbnail_url(html_source),
                ).to_dict()
        except Exception as e:
            # Handle exceptions, log errors, and set an empty dictionary
            ad_info = dict()
            current_class = self.__class__.__name__
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the extracted ad information
            return ad_info

    def extract_ads_info(self, html_source):
        """
        Extract information about multiple Facebook ads from the ads HTML source.

        Args:
            html_source (str): The HTML source code of the ads page.

        Returns:
            dict: A dictionary containing extracted ad information.
        """
        try:
            # Initialize an empty dictionary to store extracted ad information
            extracted_ads = dict()

            # Check for specific warning messages in the HTML source
            warning_messages = ['There was a problem with this request',
                                'No ads match your search criteria',
                                'Choose a category to start your search']
            if any(warning.lower() in html_source.lower() for warning in warning_messages):
                return extracted_ads

            # Parse the HTML page source using BeautifulSoup
            soup = BeautifulSoup(html_source, 'html.parser')

            # Select elements representing individual ad sections
            ad_sections = soup.select('._9ccv')

            # Iterate through the ad sections
            for ad_section in ad_sections:
                try:
                    # Select individual ad elements within the ad section
                    ad_elements = list(ad_section.select('._99s5'))

                    # Create a thread pool with the same number of threads as ad elements
                    pool = ThreadPool(len(ad_elements))

                    # Extract the prettified HTML source of each ad element
                    ad_sources = list(map(lambda e: e.prettify(), ad_elements))

                    # Extract detailed ad information for each ad in parallel
                    detailed_ads = pool.map(self.extract_detailed_ad_info, ad_sources)

                    # Update the extracted_ads dictionary with the detailed ad information
                    for detailed_ad in detailed_ads:
                        advertising_id = detailed_ad.get('advertising_id', None)
                        if advertising_id and len(detailed_ad.keys()) > 0:
                            extracted_ads[advertising_id] = detailed_ad

                except Exception as e:
                    current_class = self.__class__.__name__
                    # Log the error with class name and line number
                    error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
                    DataIO().write_to_log(error_message)

            # Process advertised product URLs and update the extracted_ads dictionary
            for advertising_id, detailed_ad in extracted_ads.items():
                advertised_url = detailed_ad.get('advertised_product_url', str())

                # Process Shopify products from the advertised URL
                adv_product = ShopifyMiddleware().process_shopify_products(advertised_url)

                # Update the detailed ad information with processed product data
                detailed_ad.update(adv_product)

        except Exception as e:
            # Handle exceptions, log errors, and set an empty dictionary
            extracted_ads = dict()
            current_class = self.__class__.__name__
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)

        finally:
            # Return the extracted ad information
            return extracted_ads

    def extract_ad_library_source(self, chrome_driver, page_id):
        """
        Extracts the source code of the Facebook Ad Library page for a given page ID.

        Args:
            chrome_driver (WebDriver): An instance of the Chrome WebDriver.
            page_id (str): The Facebook page ID.

        Returns:
            str: The HTML source code of the Ad Library page.
        """
        try:
            # Construct the URL for the Ad Library page based on the page ID
            ad_library_url = f'{settings.AD_LIBRARY_BASE_URL}{page_id}'
            
            # Print the constructed URL for debugging purposes
            print(ad_library_url)
            
            # Use the WebDriver to navigate to the Ad Library URL
            chrome_driver.get(ad_library_url)
            
            # Add a sleep delay to allow the page to load
            time.sleep(10)
            
            # Get the initial height of the webpage
            initial_height = chrome_driver.execute_script(
                'return document.documentElement.scrollHeight;')
            
            # Continuously scroll to the bottom of the webpage until it stops loading
            while True:
                # Scroll to the bottom of the webpage using JavaScript
                chrome_driver.execute_script(
                    'document.documentElement.scrollTo(0, document.documentElement.scrollHeight)')
                
                # Add a short delay after scrolling
                time.sleep(5)
                
                # Get the new height of the webpage
                new_height = chrome_driver.execute_script(
                    'return document.documentElement.scrollHeight;')
                
                # Add a short delay after getting the new height
                time.sleep(5)
                
                # If the page height remains the same, exit the loop
                if initial_height == new_height:
                    break
                
                # Update the initial height with the new height for the next iteration
                initial_height = new_height
            
            # Get the final page source code after scrolling
            page_source = chrome_driver.page_source
        except Exception as e:
            # Handle exceptions, set an empty string, and log the error
            page_source = str()
            current_class = self.__class__.__name__
            
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the extracted Ad Library page source code
            return page_source

    def extract_facebook_page_source(self, page_url=str(), chrome_driver=None):
        """
        Extracts the source code of a Facebook page given its URL.

        Args:
            page_url (str): The URL of the Facebook page.
            chrome_driver (WebDriver, optional): An instance of the Chrome WebDriver. Defaults to None.

        Returns:
            str: The HTML source code of the Facebook page.
        """
        try:
            # Initialize an empty string to store the page source
            page_source = str()

            # Check if a WebDriver instance is provided
            if chrome_driver is not None:
                # Use the WebDriver to navigate to the Facebook page URL
                chrome_driver.get(page_url)
                
                # Add a sleep delay to allow the page to load
                time.sleep(5)
                
                # Get the page source using the WebDriver
                page_source = chrome_driver.page_source
            else:
                # Load Facebook cookies from a file
                facebook_cookies = DataIO().load_cookies(settings.COOKIE_FILE_DIR)
                
                # Initialize an HTTP request with the loaded cookies
                http_request = self.initialize_request(cookie_args=facebook_cookies)
                
                # Fetch the web page using the HTTP request
                http_response = self.fetch_web_page(http_request, page_url)
                
                # Process the HTML response to get the page source
                page_source = self.process_html_response(http_response)
        except Exception as e:
            # Handle exceptions, set an empty string, and log the error
            page_source = str()
            current_class = self.__class__.__name__
            
            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the extracted Facebook page source code
            return page_source

    def convert_running_datetime(self, input_datetime):
        """
        Converts a running datetime string into a standardized datetime format.

        Args:
            input_datetime (str): The input datetime string to be converted.

        Returns:
            str: The converted datetime string in the format 'YYYY-MM-DD HH:MM:SS'.
        """
        try:
            # Initialize an empty string to store the converted datetime
            converted_datetime = str()

            # Check if the datetime string contains 'Ngày bắt đầu chạy:'
            if 'Ngày bắt đầu chạy:' in input_datetime:
                # Remove 'Ngày bắt đầu chạy:' and clean up the string
                input_datetime = input_datetime.replace(
                    'Ngày bắt đầu chạy: ', str())
                input_datetime = input_datetime.replace('Tháng', ' ').replace(',', ' ')
                input_datetime = re.sub(r'[\s]+', ' ', input_datetime)

                # Parse the datetime and convert it to the specified timezone format
                converted_datetime = datetime.strptime(input_datetime, '%d %m %Y').astimezone(
                    pytz.timezone(settings.TIMEZONE)).strftime('%Y-%m-%d %H:%M:%S')
            # Check if the datetime string contains 'Started running on'
            elif 'Started running on' in input_datetime:
                # Remove 'Started running on' and clean up the string
                input_datetime = input_datetime.replace('Started running on ', str())
                input_datetime = input_datetime.replace(',', ' ')
                input_datetime = re.sub(r'[\s]+', ' ', input_datetime)

                # Parse the datetime and convert it to the specified timezone format
                converted_datetime = datetime.strptime(input_datetime, '%b %d %Y').astimezone(
                    pytz.timezone(settings.TIMEZONE)).strftime('%Y-%m-%d %H:%M:%S')
        except Exception as e:
            # Handle exceptions, set an empty string, and log the error
            converted_datetime = str()
            current_class = self.__class__.__name__

            # Log the error with class name and line number
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message)
        finally:
            # Return the converted datetime string
            return converted_datetime


class ScraperMiddleware(ShopifyMiddleware, AdLibraryMiddleware):
    """
    Middleware for scraping Facebook page and ad information.

    This class extends the functionality of both 'ShopifyMiddleware' and 'AdLibraryMiddleware'
    to process and scrape information from Facebook pages and their ads.
    """

    def __init__(self):
        super().__init__()

    def process_scraper_output(self, facebook_page_url):
        """
        Process the output of the scraper for a Facebook page URL.

        Args:
            facebook_page_url (str): The URL of the Facebook page to scrape.

        Returns:
            dict: A dictionary containing scraped information about the Facebook page and its ads.
        """
        # Initialize dictionaries to store ad information
        ads_info_dict = dict()
        fbp_ads_info_dict = dict()

        try:
            # Initialize a Chrome WebDriver
            chrome_driver = self.initialize_chrome_driver()
            # Process Facebook cookies for the WebDriver
            chrome_driver = self.process_facebook_cookie(chrome_driver)
            # Extract the HTML source of the Facebook page
            fb_page_html_source = self.extract_facebook_page_source(facebook_page_url)
            # Check the status of the Facebook page
            page_status = self.is_page_working(fb_page_html_source)
            # Get the current time and format it
            current_time = DataIO().get_current_time().strftime('%Y-%m-%d %H:%M:%S.%f')

            # If the Facebook page is not active, return limited information
            if not page_status:
                fbp_ads_info_dict.update({
                    'facebook_page_url': facebook_page_url,
                    'page_status': page_status,
                    'last_crawling': current_time,
                })
                return fbp_ads_info_dict

            # Extract the Facebook page ID and name from the page source
            facebook_page_id = self.extract_facebook_page_id(fb_page_html_source)
            facebook_page_name = self.extract_facebook_page_name(fb_page_html_source)

            # If the Facebook page ID is empty, return limited information
            if not facebook_page_id:
                fbp_ads_info_dict.update({
                    'facebook_page_url': facebook_page_url,
                    'page_status': False,
                    'last_crawling': current_time,
                })
                return fbp_ads_info_dict

            # Extract ad library source using the Chrome WebDriver
            ad_library_html_source = self.extract_ad_library_source(chrome_driver, facebook_page_id)

            # If the Facebook page name is empty, try to extract it from the ad library source
            if not facebook_page_name:
                facebook_page_name = self.extract_facebook_page_name(ad_library_html_source)

            # Extract ad information from the ad library source
            ads_info_dict = self.extract_ads_info(ad_library_html_source)

            # Update the 'fbp_ads_info_dict' dictionary with page information
            fbp_ads_info_dict.update({
                'facebook_page_id': facebook_page_id,
                'facebook_page_name': facebook_page_name,
                'facebook_page_url': facebook_page_url,
                'page_status': page_status,
                'last_crawling': current_time,
                'facebook_ads_info': ads_info_dict
            })

        except Exception as e:
            # Handle exceptions and update 'fbp_ads_info_dict' with error information
            fbp_ads_info_dict.update({
                'facebook_page_url': facebook_page_url,
                'page_status': False
            })
            # Get the name of the current class
            current_class = self.__class__.__name__
            # Generate a log message with error details
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            # Log the error with class name and line number
            DataIO().write_to_log(error_message)
        finally:
            # Close the Chrome WebDriver
            self.close_chrome_driver(chrome_driver)
            # Return the 'fbp_ads_info_dict' dictionary
            return fbp_ads_info_dict
